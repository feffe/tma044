t=linspace(0,2*pi);
x=cos(t).^3; y=sin(t).^3;
plot(x,y)
axis equal, axis([-1.1 1.1 -1.1 1.1])
title('Asteroid')