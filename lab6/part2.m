%r = @(t)
x = @(t) cos(10.*t);
y = @(t) sin(30.*t);
t=linspace(0,2*pi,200);

plot3(x(t),y(t), t)
axis equal


%%

x = @(t) cos(10.*t).^3;
y = @(t) sin(10.*t).^3;
t=linspace(0,2*pi,200);

plot3(x(t),y(t), t)
axis equal

