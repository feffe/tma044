hold on

rho=1.2; a=3; n=20; m=50;
s=linspace(-pi,pi,n); t=linspace(0,2*pi,m);
[S,T]=meshgrid(s,t);
X=(a+rho*cos(S)).*cos(T); Y=(a+rho*cos(S)).*sin(T); Z=rho*sin(S);
surf(X,Y,Z)
axis equal
colormap(winter)
%%


rho=3; n=20; m=30;
s=linspace(0,pi,n); t=linspace(0,2*pi,m);
[S,T]=meshgrid(s,t);
X=rho*sin(S).*cos(T); Y=rho*sin(S).*sin(T); Z=rho*cos(S);
surf(X+5,Y+5,Z)
axis equal
colormap(autumn)

%%


rho=1; n=20; m=30;
s=linspace(0,pi,n); t=linspace(0,2*pi,m);
[S,T]=meshgrid(s,t);
X=rho*sin(S).*cos(T); Y=rho*sin(S).*sin(T); Z=rho*cos(S);
surf(X-3,Y-3,Z)
axis equal
colormap(autumn)
%%


axis equal vis3d
camlight left
for i=1:200
  camorbit(5,0)
  drawnow; pause(0.05)
end



%%

axis equal vis3d
h=camlight('left');
for i=1:200
  camorbit(5, 0)
  camlight(h, 'left')
  drawnow; pause(0.05)
end