a=1; b=0.5; p=0; q=0;

x= @(t) p+a.*cos(t);
y= @(t) q+b.*sin(t);
t=linspace(0,2*pi,1000);
X=x(t);
Y=y(t);
L=[X;Y];
n = numel(t);
perimeter=0;
for k=1:1:n-1 
  perimeter=perimeter+sqrt( (x(t(k+1))-x(t(k))).^2 + (y(t(k+1))-y(t(k))).^2 ); 
end

%%
f = @(t) sqrt( sin(t).^2 + (0.5.*cos(t)).^2);

len = integral(f,0,2*pi)