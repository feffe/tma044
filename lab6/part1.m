a=1; b=0.5; p=0; q=0;

r= @(t)[p+a.*cos(t), q+b.*sin(t)];

x= @(t) p+a.*cos(t);
y= @(t) q+b.*sin(t);
t=linspace(0,2*pi,200);

plot(x(t),y(t))
axis equal