[X,Y]=meshgrid(linspace(-2,2,20),linspace(-2,2,20));
quiver(X,Y,X.*Y,X-Y,0.8)
axis equal, axis([-2 2 -2 2])




F=@(t,u)[u(1).*u(2);u(1)-u(2)];
hold on
[t,U]=ode45(F,[-2 2],[-2;-1.75]);
plot(U(:,1),U(:,2),'g','linewidth',2)
[t,U]=ode45(F,[-2 2],[1.5;-2]);
plot(U(:,1),U(:,2),'g','linewidth',2)

[t,U]=ode45(F,[-2 2],[-2;-1.5]);
plot(U(:,1),U(:,2),'g','linewidth',2)

hold off