
F=@(t,u)[cos(u(1)-u(2));sin(u(1).*u(2))];
hold on
[t,U]=ode45(F,[0 5],[0;1]);
plot(U(:,1),U(:,2),'g','linewidth',2)
[t,U]=ode45(F,[0 5],[0;0.25]);
plot(U(:,1),U(:,2),'g','linewidth',2)


[X,Y]=meshgrid(linspace(0,3,15),linspace(0,3,15));
quiver(X,Y,cos(X-Y),sin(X.*Y),0.8)
axis equal, axis([0 3 0 3])
hold off