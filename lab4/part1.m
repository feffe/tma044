f=@(x,y)2.*x.^3-3.*x.^2-6.*x.*y.*(x-y-1);    % där x1=x och x2=y

%dfdx=@(x,y)x.*(1+y.^2)-1; dfdy=@(x,y)x.^2.*y;

%L=@(x,y,a,b)f(a,b)+dfdx(a,b)*(x-a)+dfdy(a,b)*(y-b);

%n=@(x,y)[dfdx(x,y);dfdy(x,y);-1];

%a=0.8; b=0.1;
%p0=[a;b;f(a,b)]; n0=n(a,b);
x=linspace(-2,2,50); y=linspace(-2,2,50);
[X,Y]=meshgrid(x,y);

Z=f(X,Y); 
%T=L(X,Y,a,b);

[px, py] = gradient(Z);

%%
surfc(x,y,Z)
hold on
%surf(x,y,T,'FaceColor','b','FaceAlpha',0.4)
%s=[-1 1];
%plot3(p0(1)+s*n0(1),p0(2)+s*n0(2),p0(3)+s*n0(3),'m','linewidth',2)
hold off
xlabel('x'), ylabel('y'), zlabel('z'), box on
%axis equal, axis vis3d, rotate3d on

%%
subplot(1,2,1);
surfc(X,Y,Z, 'FaceColor', 'flat' );
subplot(1,2,2);
contourf(X,Y,Z,150)
hold on
quiver(X,Y,px,py)
hold off