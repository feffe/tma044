f=@(x)2.*x(1).^3-3.*x(1).^2-6.*x(1).*x(2).*(x(1)-x(2)-1);    % där x1=x och x2=y

dfdx=@(x,y) 6.*(x.^2-2.*x.*y-x+y.^2+y);
dfdy=@(x,y) -6.*x.*(x-2.*y-1);


Df=@(x) [dfdx(x(1),x(2)), dfdy(x(1),x(2))];

fimplicit(dfdx, ':r')

hold on
fimplicit(dfdy, 'g')




%%

x0=[0; 0];
x1=[-1; -1];
x2=[0; -1];
x3=[1; 0];
kmax=10; tol=0.5e-8;


%%
for k=1:kmax
  h=-Df(x0)\f(x0);
  x0=x0+h;
  disp([x0' norm(h)])
  if norm(h)<tol, break, end
end
% 0 0 0

%%
for k=1:kmax
  h=-Df(x1)\f(x1);
  x1=x1+h;
  disp([x1' norm(h)])
  if norm(h)<tol, break, end
end
% -1 -1 0

%%
for k=1:kmax
  h=-Df(x2)\f(x2);
  x2=x2+h;
  disp([x2' norm(h)])
  if norm(h)<tol, break, end
end
% 0 -1 0

%%
for k=1:kmax
  h=-Df(x3)\f(x3);
  x3=x3+h;
  disp([x3' norm(h)])
  if norm(h)<tol, break, end
end
% 1 0 0


%%
syms x y
%%
x=0; y=0;
%%
x=-1; y=-1;
%%
x=0; y=-1;
%%
x=1; y=0;
%%
%HESSIAN
h = [12*x-12.*y-6,                  -6.*(x.*y-1)-6.*x+y;
          -6.*(x.*y-1)-6.*x+6.*y,   12.*x];

eig(h)