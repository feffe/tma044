f = @(x, y) x.*y.*exp(-(x.^2+y.^2));


x=linspace(-2,2,20); y=linspace(-2,2,20);
[X,Y]=meshgrid(x,y);

Z=f(X,Y); 
%%
subplot(1,2,1);
surfc(X,Y,Z, 'FaceColor', 'flat' );
subplot(1,2,2);
contourf(X,Y,Z,30)

%%

f = @(x) x(1).*x(2).*exp(-(x(1).^2+x(2).^2));
df = @(x) [(x(2)-2.*x(1).^2.*x(2)).*exp(-(x(1).^2+x(2).^2));
            (x(1)-2.*x(1).*x(2).^2).*exp(-(x(1).^2+x(2).^2))];
contourf(X,Y,Z,30)
hold on

%%
x0 = [-1.75;
      1];
plot(x0(1), x0(2), 'ro');

%%
g = @(s) f(x0-s.*df(x0));
s = fminbnd(g, 0, 0.4);
x0 = x0-s*df(x0);
plot(x0(1), x0(2), 'ro');
