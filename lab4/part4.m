f = @(x,y) x.^3+3.*x.*y.^2-15.*x+y.^2-15.*y;
dfdx = @(x,y) 3.*(x.^2+y.^2-5);
dfdy = @(x,y) (6.*x+2).*y-15;



x=linspace(-10,10,50); y=linspace(-10,10,50);
[X,Y]=meshgrid(x,y);

Z=f(X,Y); 



%%
subplot(1,2,1);
surfc(X,Y,Z, 'FaceColor', 'flat' );
subplot(1,2,2);
contourf(X,Y,Z,30)

%%

fimplicit(dfdx, ':r')

hold on
fimplicit(dfdy, 'g')
%x=1.9, y=1.1    x=0.9, y=2

%%
f = @(x) x(1).^3+3.*x(1).*x(2).^2-15.*x(1)+x(2).^2-15.*x(2);
df = @(x) [dfdx(x(1),x(2));
          dfdy(x(1),x(2))];

%%


x0 = [1.9; 1.1]; x1 = [0.9; 2];
a=fsolve(df, x0);
% 1.9495   1.0951
b=fsolve(df, x1);
% 0.8838   2.0540
plot(a(1), a(2),'ro');
plot(b(1), b(2),'ro');


%%
syms x y
f = x.^3+3.*x.*y.^2-15.*x+y.^2-15.*y;
hessian(f,[x,y])
%  [ 6*x,     6*y]
%  [ 6*y, 6*x + 2]  
%%
h=@(x,y) [6.*x, 6.*y;
          6.*y, 6.*x+2];
%%
eig(h(a(1), a(2)))
% positive both, minimum point
eig(h(b(1), b(2)))
% mixed, indefinite, saddlepoint

% positive if all positive, minimum point
% negative if all negative, maximum point
% mixed indefinite, saddlepoint




% fattar inte riktigt grejen med att använda fminunc, det bidrar ingenting verkar det som, den informationen som det ger har redan tagits fram?