f=@(x)cos(x)-x; Df=@(x)-sin(x)-1;
x=1;
kmax=10; tol=0.5e-8;
for k=1:kmax
  h=-f(x)/Df(x);
  x=x+h;
  disp([x h])
  if abs(h)<tol, break, end
end