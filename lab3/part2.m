% plot both functions to find approximation start points

f1=@(x, y) (sin(x)-y);


%f1 = @(x,y) f([x,y])(1);
f2=@(x, y) (x-cos(y));


%k =  [-10 : 0.01: 10];
%y = [-10 : 0.01: 10];


fimplicit(f1, ':r')

hold on
fimplicit(f2, 'g')



%%

f=@(x)[sin(x(1))-x(2); x(1)-cos(x(2))];

x0=[0.79; 0.67];


x=fsolve(f, x0);
% [0.768169169829838;0.694819703234553]
%%
plot(0.768169169829838, 0.694819703234553, 'ro')