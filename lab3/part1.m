% plot both functions to find approximation start points

f1=@(x, y) (x.^3+y.^2-1);


%f1 = @(x,y) f([x,y])(1);
f2=@(x, y) (exp(x.*y)+x+y-2);


%k =  [-10 : 0.01: 10];
%y = [-10 : 0.01: 10];


fimplicit(f1, ':r')

hold on
fimplicit(f2, 'g')



%%

f=@(x)[x(1).^3+x(2).^2-1; exp(x(1).*x(2))+x(1)+x(2)-2];

df=@(x)([3.*x(1).^2,              2.*x(2);
        1+x(2).*exp(x(1).*x(2)),  1+x(1).*exp(x(1).*x(2))]);


x0=[-1; -1.43];
x1=[1; 0];
x2=[0; 1];
x3=[-2.8; 4.8];

xlist = [x0, x1, x2, x3];

kmax=10; tol=0.5e-8;

%%

for k=1:kmax
  h=-df(x0)\f(x0);
  x0=x0+h;
  disp([x0' norm(h)])
  if norm(h)<tol, break, end
end
% Svar x0: -1.0342,   -1.4512,    0.0000

%%
for k=1:kmax
  h=-df(x1)\f(x1);
  x1=x1+h;
  disp([x1' norm(h)])
  if norm(h)<tol, break, end
end
% Svar x1: 1, 0, 0

%%
for k=1:kmax
  h=-df(x2)\f(x2);
  x2=x2+h;
  disp([x2' norm(h)])
  if norm(h)<tol, break, end
end

% Svar x2: 0, 1, 0

%%
for k=1:kmax
  h=-df(x3)\f(x3);
  x3=x3+h;
  disp([x3' norm(h)])
  if norm(h)<tol, break, end
end

% Svar x3: -2.8063 ,   4.8063 ,  0.0000