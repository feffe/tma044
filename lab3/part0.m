f1=@(x1,x2)x1.*(1+x2.^2)-1;
f2=@(x1,x2)x2.*(1+x1.^2)-2;

f=@(x)[f1(x(1),x(2))
      f2(x(1),x(2))];

x1=linspace(-1,3,30); x2=linspace(-1,3,30);
[X1,X2]=meshgrid(x1,x2);
Z1=f1(X1,X2); Z2=f2(X1,X2);
contour(x1,x2,Z1,[0 0],'g');
hold on;
contour(x1,x2,Z2,[0 0],'b');
grid on;