f=@(x,y)[x.^3+y.^2-1; exp(1)^(x.*y)+x+y-2];    % där x1=x och x2=y

df1=@(x,y)[3*x.^2+y.^2, x.^3+2*y];
df2=@(x,y)[y.*exp(1)^(x.*y)+y+1, x.*exp(1)^(x.*y)+x+1];
%df1dx= 3x³+y²  df1dy= x³+2y  df2dx= y*e^(xy)+y+1   df2dy= xe^(xy)+x+1

L=@(x,y,a,b)f(a,b)+[df1(a,b); df2(a,b)]*[(x-a); (y-b)];

%n=@(x,y)[dfdx(x,y); dfdy(x,y); -1, -1];

a=3; b=1;
p0=[a;b;f(a,b)]; n0=n(a,b);
x=linspace(-3,4,40); y=linspace(-3,3,40);
[X,Y]=meshgrid(x,y);

Z=f(X,Y); 
T=L(X,Y,a,b);

surf(x,y,Z)
hold on
surf(x,y,T,'FaceColor','b','FaceAlpha',0.4)
s=[-1 1];
plot3(p0(1)+s*n0(1),p0(2)+s*n0(2),p0(3)+s*n0(3),'m','linewidth',2)
hold off
xlabel('x'), ylabel('y'), zlabel('z'), box on
axis equal, axis vis3d, rotate3d on