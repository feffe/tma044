f=@(x,y)0.5*x.^2.*(1+y.^2)-x+1;    % där x1=x och x2=y

dfdx=@(x,y)x.*(1+y.^2)-1; dfdy=@(x,y)x.^2.*y;

L=@(x,y,a,b)f(a,b)+dfdx(a,b)*(x-a)+dfdy(a,b)*(y-b);

n=@(x,y)[dfdx(x,y);dfdy(x,y);-1];

a=0.8; b=0.1;
p0=[a;b;f(a,b)]; n0=n(a,b);
x=linspace(-0.5,1.5,20); y=linspace(-1.2,1.2,20);
[X,Y]=meshgrid(x,y);

Z=f(X,Y); T=L(X,Y,a,b);

surf(x,y,Z)
hold on
surf(x,y,T,'FaceColor','b','FaceAlpha',0.4)
s=[-1 1];
plot3(p0(1)+s*n0(1),p0(2)+s*n0(2),p0(3)+s*n0(3),'m','linewidth',2)
hold off
xlabel('x'), ylabel('y'), zlabel('z'), box on
axis equal, axis vis3d, rotate3d on