f=@(x,y)(x.*cos(2*x).*sin(y));    % där x1=x och x2=y

dfdx=@(x,y)(sin(y).*(cos(2*x)-2*x.*sin(2*x))); dfdy=@(x,y)(x*cos(2*x).*cos(y));

L=@(x,y,a,b)f(a,b)+dfdx(a,b)*(x-a)+dfdy(a,b)*(y-b);

n=@(x,y)[dfdx(x,y);dfdy(x,y);-1];

a=3; b=1;
p0=[a;b;f(a,b)]; n0=n(a,b);
x=linspace(-3,4,40); y=linspace(-3,3,40);
[X,Y]=meshgrid(x,y);

Z=f(X,Y); T=L(X,Y,a,b);

surf(x,y,Z)
hold on
surf(x,y,T,'FaceColor','b','FaceAlpha',0.4)
s=[-1 1];
plot3(p0(1)+s*n0(1),p0(2)+s*n0(2),p0(3)+s*n0(3),'m','linewidth',2)
hold off
xlabel('x'), ylabel('y'), zlabel('z'), box on
axis equal, axis vis3d, rotate3d on