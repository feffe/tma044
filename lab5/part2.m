f = @(x, y) y.*sin(y+x.*y);
A = [0:0.1:1];
B = [-pi/2:0.1:pi/2];

h = (A(end)-A(1))/numel(A);
k = (B(end)-B(1))/numel(B);
[X,Y] = meshgrid(A,B);
Z0=arrayfun(f,X,Y);
Z1=arrayfun(f,X-h,Y-k);
Z2=arrayfun(f,X-h/2,Y-k/2);

right=sum(sum(Z0)).*h.*k %höger
left=sum(sum(Z1)).*h.*k %vänster
mid=sum(sum(Z2)).*h.*k %vänster
trapp=trapz(B,(trapz(A, Z0, 2))) %trapp
%trapp2=trapz(trapz(Z0)).*h.*k % ??????  


%%
syms x y
integral2(f, 0, 1, -pi/2, pi/2)