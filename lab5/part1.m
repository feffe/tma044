%  q = integral(fun,xmin,xmax)
syms x
f = @(x) x.*sin(x);
integral(f, 0, 1)
%%
%F = @(x) sin(x)-x.*cos(x);
f = @(x) x.*sin(x);

sum([f(0),f(1)]) % höger
sum([f(0-1),f(1-1)]) % vänster
sum([f(0-0.5),f(1-0.5)]) % mitt
%%
f = @(x) x.*sin(x);
A = [0:0.0001:1];
h = (A(end)-A(1))/numel(A);
%B = [0-1:1-1];
%C = [0-0.5:1-0.5];
hoger=sum(arrayfun(f, A)).*h %höger
vanster=sum(arrayfun(f, A-h)).*h %vänster
mitt=sum(arrayfun(f, A-h/2)).*h %mitt
trapp=trapz(arrayfun(f, A)).*h %trapezoid rule aka trappstegsregeln