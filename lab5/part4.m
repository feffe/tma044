syms a
%f = @(x, y) a-x+y;
f = @(x, y) a+y;
xmin = @(y) -a;
xmax = @(y) a;
ymax = @(x) a-(x.^2)/a;
int2 = integral2(f, xmin, xmax, 0, ymax) 

% can't use symbolic, otherwise this is the solution, dunno what to do about it, the instructions are unclear.
% it says to solve with "integral2" but using syms in that is forbidden *shrug*

%%
