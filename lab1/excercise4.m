f=@(x,y)(exp(1)^(-2*(x.^2+y.^2))*(-x).*y);
x=linspace(-2,2,100); y=linspace(-2,2,100);
[X,Y]=meshgrid(x,y);
Z=f(X,Y);
contourf(X,Y,Z,30);
grid on, box on;
xlabel('x'), ylabel('y'), zlabel('z = f(x,y)');
daspect([1 1 1])
set(gca,'BoxStyle','full','Box','on')