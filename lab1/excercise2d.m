f=@(x,y)( (exp(1))^( sqrt((x.^2)+(y.^2) )).*y);
x=linspace(-2,2,30); y=linspace(-2,2,30);
[X,Y]=meshgrid(x,y);
Z=f(X,Y);
surf(X,Y,Z);
grid on, box on;
xlabel('x'), ylabel('y'), zlabel('z = f(x,y)');
