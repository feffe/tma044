f=@(x,y)(-6*y./(2+x.^2+y.^2));
x=linspace(-6,6,100); y=linspace(-6,6,100);
[X,Y]=meshgrid(x,y);
Z=f(X,Y);
surf(X,Y,Z);
grid on, box on;
xlabel('x'), ylabel('y'), zlabel('z = f(x,y)');
daspect([1 1 1])
set(gca,'BoxStyle','full','Box','on')