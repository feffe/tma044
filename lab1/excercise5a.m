f=@(x,y)(x+2*y-2);
x=linspace(1,2,30); y=linspace(3,4,30);
[X,Y]=meshgrid(x,y);
Z=f(X,Y);
subplot(1,2,1);
surfc(X,Y,Z, 'FaceColor', 'flat' );
subplot(1,2,2);
contourf(X,Y,Z,30);
grid on, box on;
xlabel('x'), ylabel('y'), zlabel('z = f(x,y)');
daspect([1 1 1])
set(gca,'BoxStyle','full','Box','on')
