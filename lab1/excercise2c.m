f=@(x,y)((1-y.^2).^(1/2));
x=linspace(0,3,30); y=linspace(-1,1,30);
[X,Y]=meshgrid(x,y);
Z=f(X,Y);
surf(X,Y,Z);
grid on, box on;
xlabel('x'), ylabel('y'), zlabel('z = f(x,y)');
