f=@(x,y)(x+2*y-2);
% (x, y)∈[1,2]×[3,4], unclear on the usage of these symbols, seems weird. cross products can't be applied to vectors in R2.
% Gonna go forward with assumption they mean 1 <= x <= 2 and 3 <= y <=  4
x=linspace(1,2,30); y=linspace(3,4,30);
[X,Y]=meshgrid(x,y);
Z=f(X,Y);
surf(X,Y,Z);
grid on, box on;
xlabel('x'), ylabel('y'), zlabel('z = f(x,y)');
